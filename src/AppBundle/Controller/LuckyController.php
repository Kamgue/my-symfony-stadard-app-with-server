<?php
// src/AppBundle/Controller/LuckyController.php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LuckyController extends Controller
{
    /**
     * @Route("/lucky")
     */
    public function indexAction()
    {
        // retrieve the object from database
        $product = "";
        if (!$product) {
            throw $this->createNotFoundException('The product does not exist');
        }
        // redirects to the "homepage" route
        return $this->redirectToRoute('homepage');

        // does a permanent - 301 redirect
        return $this->redirectToRoute('homepage', [], 301);

        // redirects to a route with parameters
        return $this->redirectToRoute('blog_show', ['slug' => 'my-page']);

        // redirects to a route and maintains the original query string parameters
        return $this->redirectToRoute('blog_show', $request->query->all());

        // renders app/Resources/views/lucky/number.html.twig
        //return $this->render('lucky/number.html.twig', ['number' => $number]);

        // redirects externally
        //return new RedirectResponse($this->generateUrl('homepage'));
        return $this->redirect('http://symfony.com/doc');
    }
    /**
     * @Route("/lucky/number")
     */
    public function numberAction()
    {
        $number = random_int(0, 100);

        return $this->render('lucky/number.html.twig', [
            'number' => $number,
        ]);
    }
    /**
     * @Route("/lucky/n")
     */
    public function numberActio()
    {
        $number = random_int(0, 100);

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }
    /**
     * @Route("/salo")
     */
    public function numberAio()
    {
        $number = random_int(0, 100);

        return new Response('salo');
    }
    /**
     * @Route("/lucky/number/{max}", requirements={"max"="\d+"})
     */
    public function numberActions($max, LoggerInterface $logger)
    {
        //php bin/console debug:autowiring
        $logger->info('We are logging!');
        $number = random_int(0, $max);

        return new Response('<html><body>Lucky number: '.$number.'</body></html>');
    }

    /**
     * @Route("/lucky/request")
     */
    public function indexActionResquest(Request $request)
    {
        //$page = $request->query->get('page', 1);

        return new Response($request);
    }

    /**
     * @Route("/lucky/session")
     */
    public function indexActionsession(SessionInterface $session)
    {
        // stores an attribute for reuse during a later user request
        $session->set('foo', 'bar');

        // gets the attribute set by another controller in another request
        $foobar = $session->get('foobar');

        // uses a default value if the attribute doesn't exist
        $filters = $session->get('filters', []);

        //return new Response($session);
    }

    /**
     * @Route("/lucky/update")
     */
    public function updateAction(Request $request)
    {
        // ...

        if ($form->isSubmitted() && $form->isValid()) {
            // do some sort of processing

            $this->addFlash(
                'notice',
                'Your changes were saved!'
            );
            // $this->addFlash() is equivalent to $request->getSession()->getFlashBag()->add()

            //return $this->redirectToRoute(...);
        }

        //return $this->render(...);
    }

    /**
     * @Route("/lucky/request/response")
     */
    public function indexActionrespon(Request $request)
    {
        $request->isXmlHttpRequest(); // is it an Ajax request?

        $request->getPreferredLanguage(['en', 'fr']);

        // retrieves GET and POST variables respectively
        $request->query->get('page');
        $request->request->get('page');

        // retrieves SERVER variables
        $request->server->get('HTTP_HOST');

        // retrieves an instance of UploadedFile identified by foo
        $request->files->get('foo');

        // retrieves a COOKIE value
        $request->cookies->get('PHPSESSID');

        // retrieves an HTTP request header, with normalized, lowercase keys
        $request->headers->get('host');
        $request->headers->get('content-type');
        // returns '{"username":"jane.doe"}' and sets the proper Content-Type header
        return $this->json(['username' => 'jane.doe']);

        // the shortcut defines three optional arguments
        // return $this->json($data, $status = 200, $headers = [], $context = []);
        }
}