<?php
// app/config/routing.php
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$routes = new RouteCollection();
// Matches /blog exactly
$routes->add('blog_list', new Route('/blog', [
    '_controller' => 'AppBundle:Blog:list',
]));
// Matches /blog/*
// but not /blog/slug/extra-part
$routes->add('blog_show', new Route('/blog/{slug}', [
    '_controller' => 'AppBundle:Blog:show',
]));

return $routes;
